#!/bin/bash


hr=$(date +%H%M)


sd="$HOME"

dd="/tmp/backup"


mkdir -p $dd

bf="backup_$hr.tar.gz"

cp -r $sd/* $dd

tar -czf $dd/$bf -C $dd .

find $dd -type f ! -name $bf -delete
find $dd -type d ! -name . -delete


