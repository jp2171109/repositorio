#!/bin/bash


echo "Digite 4 nomes para criar diretórios:"
read -p "Nome 1: " nome1
read -p "Nome 2: " nome2
read -p "Nome 3: " nome3
read -p "Nome 4: " nome4


mkdir "$nome1"

d=$(date +%d-%m-%Y)

echo "# $nome1" > "$nome1/README.md"
echo "Data: $d" >> "$nome1/README.md"
echo "Diretório '$nome1' criado com sucesso."


mkdir "$nome2"

echo "# $nome2" > "$nome2/README.md"
echo "Data: $d" >> "$nome2/README.md"
echo "Diretório '$nome2' criado com sucesso."


mkdir "$nome3"

echo "# $nome3" > "$nome3/README.md"
echo "Data: $d" >> "$nome3/README.md"
echo "Diretório '$nome3' criado com sucesso."


mkdir "$nome4"

echo "# $nome4" > "$nome4/README.md"
echo "Data: $d" >> "$nome4/README.md"
echo "Diretório '$nome4' criado com sucesso."

