#!/bin/bash

SOURCE_DIR="/home/aluno"
DEST_DIR="/home/backup1"
LOG_FILE="/home/bacup2"
DATE=$(date +"%Y-%m-%d_%H-%M-%S")
BACKUP_FILE="backup_$DATE.tar.gz"
FILE_PATTERN=".*\.(txt|log)$"

log() {
    echo "[$(date +"%Y-%m-%d %H:%M:%S")] $1" >> "$LOG_FILE"
}


if [ ! -d "$SOURCE_DIR" ]; then
    log "ERRO: O diretório de origem não existe!"
    exit 1
fi

if [ ! -d "$DEST_DIR" ]; then
    log "ERRO: O diretório de destino não existe!"
    exit 1
elif [ ! -w "$DEST_DIR" ]; then
    log "ERRO: Permissão negada para escrever no diretório de destino!"
    exit 1
fi

AVAILABLE_SPACE=$(df "$DEST_DIR" | awk 'NR==2 {print $4}')
if [ "$AVAILABLE_SPACE" -lt 5000000 ]; then
    log "ERRO: Espaço em disco insuficiente para o backup!"
    exit 1
fi


FILE_LIST=$(find "$SOURCE_DIR" -type f | awk "/$FILE_PATTERN/")

if [ -z "$FILE_LIST" ]; then
    log "ERRO: Nenhum arquivo correspondente ao padrão encontrado para backup."
    exit 1
fi


tar -czf "$DEST_DIR/$BACKUP_FILE" -C "$SOURCE_DIR" $(echo "$FILE_LIST" | awk '{print $NF}' | sed "s|^$SOURCE_DIR/||")


if [ $? -eq 0 ]; then
    log "Backup criado com sucesso: $DEST_DIR/$BACKUP_FILE"
else
    log "ERRO: Falha ao criar o backup."
    exit 1
fi
