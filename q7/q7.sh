#!/bin/bash


t=$(date +%Y-%m-%d)


s=$(date +%u)


d=$(( (3 - s + 7) % 7 ))

# Calcula as datas das duas próximas quartas-feiras
x=$(date -d "$t + $d days" +%Y-%m-%d)
y=$(date -d "$x + 7 days" +%Y-%m-%d)


echo "As duas próximas quartas-feiras são em:"
echo "1. $x"
echo "2. $y"

