#!/bin/bash


echo "=== Informações do Sistema ==="
echo "Hostname: $(hostname)"
echo "Sistema Operacional: $(uname -a)"
echo "Distribuição: $(lsb_release -d | cut -f2)"


echo -e "\n=== CPU ==="
echo "Modelo: $(grep "model name" /proc/cpuinfo | uniq | cut -d ":" -f2)"
echo "Número de núcleos: $(grep -c "^processor" /proc/cpuinfo)"
echo "Arquitetura: $(uname -m)"


echo -e "\n=== Memória ==="
echo "Total: $(free -h | awk '/Mem/{print $2}')"
echo "Usada: $(free -h | awk '/Mem/{print $3}')"
echo "Disponível: $(free -h | awk '/Mem/{print $7}')"

echo -e "\n=== Discos ==="
df -h | grep '^/dev/'


echo -e "\n=== Interfaces de Rede ==="
ip addr show


echo -e "\n=== Outras Informações ==="
echo "Usuários Logados:"
who
echo "Processos em Execução:"
ps aux
